using Test

#Função de testes para a função palindromo
function test()
        @test palindromo("")
        @test palindromo("ovo")
        @test palindromo("A mãe te ama.")
        @test palindromo("Socorram-me, subi no ônibus em Marrocos!")
        @test !palindromo("Passei em MAC0110!")
        @test !palindromo("MiniEP11")
end

using Unicode
function palindromo(string)
        #Primeira correção da string com remoção de alguns acentos e de maisuculas com ajuda da biblioteca Unicode
        stringClean = Unicode.normalize(string, stripmark=true, casefold=true)

        #Lista de caracteres indesejados alem do retirados anteriormente
        removeList = ["!",",","-"," ","."]

        #Remoção de todos caracteres da removeList presentes na stringClean
        for i in removeList
                stringClean = replace(stringClean, i=>"")
        end

        #Caso a stringClean seja igual ao seu inverso, então a frase e um palindromo
        return reverse(stringClean) == stringClean
end

#test()
